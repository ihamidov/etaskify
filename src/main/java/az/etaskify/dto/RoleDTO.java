package az.etaskify.dto;

import lombok.Data;

@Data
public class RoleDTO {

    private Long id;
    private String name;
}
