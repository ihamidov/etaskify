package az.etaskify.dto;


import az.etaskify.validator.PasswordMatches;
import az.etaskify.validator.ValidPassword;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@Builder
@PasswordMatches
public class UserDTO {

    @NotBlank
    @Size(min = 4, max = 20,message = "Username's size must be between 4 and 40")
    private String username;

    @NotBlank
    @Size(min = 5,max = 50,message = "Email's size must be between 5 and 50")
    @Email
    private String email;

    @NotBlank
    private String name;

    @NotBlank
    private String surname;

    private Set<String> authority;

    @NotBlank
    @Size(min = 6, max = 40,message = "Password's size must be between 6 and 40")
    @ValidPassword
    private String password;

    @Transient
    private String matchingPassword;


    void addAuthority(String authorities) {
        this.authority.add(authorities);
    }

}
