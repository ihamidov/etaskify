package az.etaskify.service.impl;

import az.etaskify.domain.Authority;
import az.etaskify.domain.Role;
import az.etaskify.domain.User;
import az.etaskify.dto.UserDTO;
import az.etaskify.payload.request.LoginRequest;
import az.etaskify.payload.response.JwtResponse;
import az.etaskify.payload.response.MessageResponse;
import az.etaskify.repository.AuthorityRepository;
import az.etaskify.repository.UserRepository;
import az.etaskify.security.JwtUtils;
import az.etaskify.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final JwtUtils jwtUtils;

    private final PasswordEncoder encoder;

    private final AuthenticationManager authenticationManager;

    private final UserRepository userRepository;

    private final AuthorityRepository authorityRepository;

    private final ModelMapper modelMapper;


    @Override
    public ResponseEntity<JwtResponse> loginUser(LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles));
    }

    @Override
    @Transactional
    public ResponseEntity<MessageResponse> registrationUser(UserDTO dto) {
        if (userRepository.existsByUsername(dto.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(dto.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        // Create new user's account
        User user = new User(dto.getUsername(),
                dto.getEmail(),
                encoder.encode(dto.getPassword()));

        Set<String> strRoles = dto.getAuthority();
        Set<Authority> authorities = new HashSet<>();

        System.out.println(user);
        System.out.println(strRoles);

        if (strRoles.isEmpty()) {
            Authority authority = authorityRepository.findByName(Role.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            authorities.add(authority);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Authority adminAuthority = authorityRepository.findByName(Role.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Authority is not found."));
                        authorities.add(adminAuthority);

                        break;
                    case "user":
                        Authority userAuthority = authorityRepository.findByName(Role.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Authority is not found."));
                        authorities.add(userAuthority);

                        break;
                }
            });
        }

        user.setAuthorities(authorities);
        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    @Override
    public List<UserDTO> getUsers() {
        return userRepository.findAll().stream()
                .map(user -> modelMapper.map(user, UserDTO.class))
                .collect(Collectors.toList());
    }
}
