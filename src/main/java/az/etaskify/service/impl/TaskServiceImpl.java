package az.etaskify.service.impl;

import az.etaskify.domain.Status;
import az.etaskify.domain.Task;
import az.etaskify.domain.User;
import az.etaskify.dto.TaskDto;
import az.etaskify.exceptions.UserNotFoundException;
import az.etaskify.repository.TaskRepository;
import az.etaskify.repository.UserRepository;
import az.etaskify.service.EmailService;
import az.etaskify.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final UserRepository userRepository;
    private final TaskRepository taskRepository;
    private final ModelMapper modelMapper;
    private final EmailService emailService;


    @Override
    public void save(TaskDto taskDto) {
        if(!taskDto.getUsersIds().isEmpty()) {
            Set<User> users = taskDto.getUsersIds().stream()
                    .map(id -> {
                        return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
                    }).collect(Collectors.toSet());
            users.stream()
                    .forEach(user -> {
                        Task task = modelMapper.map(taskDto, Task.class);
                        task.setStatus(Status.NEW);
                        task.setUsers(users);
                        taskRepository.save(task);
                        emailService.sendSimpleMessage(user.getEmail(),task.getTitle(),task.getDescription());
                    });
        } else {
            Task task = modelMapper.map(taskDto, Task.class);
            task.setStatus(Status.NEW);
            taskRepository.save(task);
        }

    }

    @Override
    public List<TaskDto> getTasks() {
        return taskRepository.findAll().stream()
                .map(task -> modelMapper.map(task, TaskDto.class))
                .collect(Collectors.toList());
    }
}
