package az.etaskify.service;

import az.etaskify.dto.TaskDto;

import java.util.List;
import java.util.Set;

public interface TaskService {

    void save(TaskDto taskDto);
    List<TaskDto> getTasks();
}
