package az.etaskify.service;

import az.etaskify.dto.UserDTO;
import az.etaskify.payload.request.LoginRequest;
import az.etaskify.payload.response.JwtResponse;
import az.etaskify.payload.response.MessageResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {

    ResponseEntity<JwtResponse> loginUser(LoginRequest loginRequest);
    ResponseEntity<MessageResponse> registrationUser(UserDTO dto);
    List<UserDTO> getUsers();
}
