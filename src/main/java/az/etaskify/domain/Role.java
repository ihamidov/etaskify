package az.etaskify.domain;

public enum Role {
    ROLE_ADMIN,
    ROLE_USER
}
